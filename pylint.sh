#!/bin/bash

if ! which pylint; then
    echo "ERROR, no pylint installed." > /dev/stderr
    exit 1
fi

pylint --rcfile pylint.rc --ignore test hcs_utils 2> /dev/null | tee pylint.last

