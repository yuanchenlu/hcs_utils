About
=====

This is my personal collection of snippets that I use in multiple projects.
Each one feels a bit to small to merit it's own library.

So what does it include?

* memoize decorator (caching)
* a storage class wrapping a dict.
* unittest assert functions.
* Functions for handling large JSON documents.
* list_dir(path) –> (dirnames, filenames)
* PID file locking
* A synchronization decorator

Feedback and getting involved
-----------------------------

Send feedback and bug reports by email to hcs at furuvik dot net.

- Code Repository: http://gitorious.org/hcs_utils

